########### Project Envirement ###########

PROJECTPATH = $(shell pwd)
OUT		  	=  $(shell basename $(PROJECTPATH))
VERSION	 	=  $(shell $(PROJECTPATH)/version.sh)
ARCHITECTURE = $(shell dpkg --print-architecture)

CC 		  	= g++

CPPFLAGS 	= 


########### Project Directories ###########

SRCDIR	 	= src/
BINDIR		= build/
BUILDPATH	= $(BINDIR)$(ARCHITECTURE)/
INSTALLDIR	= /usr/lib/
HEADERDIR	= /usr/include/

SOURCES		= $(shell find $(SRCDIR) -type f | grep cpp | cut -f 1 -d '.')
DIRSTRUCTURE = $(shell find $(SRCDIR) -type d)
INCSTRUCTURE = $(patsubst %, -I%, $(DIRSTRUCTURE))
LOG_LEVEL 	= ALL

DEPS 		= 


COLOR=\033[0;32m
NC=\033[0m # No Color

#################################################################################

all:
	./prebuild.sh
	make debug
	make release


debug: CC 			+= -g
debug: BUILDPATH	:= $(BUILDPATH)dbg/
debug: OBJDIR 		:= $(BUILDPATH)obj/
debug: OBJECTS 		= $(SOURCES:%.cpp=$(OBJDIR)%.o)
debug:  $(OUT) 

release: CC 			+= -O3
release: BUILDPATH		:= $(BUILDPATH)rel/
release: OBJDIR 		:= $(BUILDPATH)obj/
release: OBJECTS 		= $(SOURCES:%.cpp=$(OBJDIR)%.o)
release: $(OUT)


$(OUT): $(SOURCES)
	$(CC) '-DVERSION="$(VERSION)"' -o $(BUILDPATH)$@ $(CPPFLAGS) $(INCSTRUCTURE) $(HEADER_DEPS) -I$(HEADERDIR) $(OBJDIR)* main.cpp $(DEPS) #-DMAX_LOG_LEVEL=$(LOG_LEVEL)
	$(CC) -shared -o $(BUILDPATH)lib$(OUT).so $(INCSTRUCTURE) '-DVERSION="$(VERSION)"' $(OBJDIR)*
	@echo


$(SOURCES): $(INCDIR)$(@.hpp) $(SRCDIR)$@ 
	@echo "${COLOR}$(OBJDIR)$(notdir $@).o${NC}:"
	$(CC) '-DVERSION="$(VERSION)"' -c -fPIC $(CPPFLAGS) $(INCSTRUCTURE) $(HEADER_DEPS) -I$(HEADERDIR) $(@).cpp -o $(OBJDIR)$(notdir $@).o -DMAX_LOG_LEVEL=$(LOG_LEVEL)
	@echo 
	
test:
	@$(MAKE) ${OUT} -B
	@$(MAKE) r

clean: 
	rm -fr $(BINDIR)*
	./prebuild.sh

remove:
	rm -rf $(HEADERDIR)$(OUT)  
	rm ${INSTALLDIR}lib${OUT}.so


install: HEADERS = $(shell find $(SRCDIR) -name '*.hpp')
install: HEADERS := $(patsubst src/%, %, $(HEADERS))
install:
	cp $(BUILDPATH)rel/lib${OUT}.so ${INSTALLDIR}lib${OUT}.so
	#ln -sf ${INSTALLDIR}lib${OUT}.so ${INSTALLDIR}lib${OUT}.so
	mkdir -p $(HEADERDIR)$(OUT)/
	cd $(SRCDIR); cp $(HEADERS) $(HEADERDIR)$(OUT)/
	

version:
	@echo $(DEPS)