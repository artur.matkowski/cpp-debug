#include <iostream>

//#include <fstream>
//#include <thread>
//#include <chrono>
#include "Logger.hpp"


// ********** Test main **********
int main() {
    // Set output to stderr
    debug::log::SetOutput(std::cerr);

    // Register topics
    debug::log::RegisterTopic("NETWORK");
    debug::log::RegisterTopic("UI");
    debug::log::RegisterTopic("DATABASE");

    // Test stable reference:
    std::ostream& net_dbg = debug::log::GetStream(debug::LogLevel::Debug, "NETWORK");
    net_dbg << "Network initialized\n";

    // Test multiple log levels:
    debug::log::error("NETWORK") << "Network error message\n";
    debug::log::warning("NETWORK") << "Network warning message\n";
    debug::log::info("UI") << "UI info message\n";
    debug::log::debug("DATABASE") << "Database debug message\n";

    // Runtime enable/disable:
    debug::log::SetLevelEnabled(debug::LogLevel::Error, false);
    debug::log::error("NETWORK") << "This error won't appear at runtime disabled.\n";
    debug::log::SetLevelEnabled(debug::LogLevel::Error, true);

    debug::log::SetTopicEnabled("UI", false);
    debug::log::info("UI") << "This won't appear (topic disabled).\n";
    debug::log::SetTopicEnabled("UI", true);
    debug::log::info("UI") << "UI topic enabled again.\n";

    // Unregistered topic
    debug::log::info("NOT_REGISTERED") << "Unregistered topic error.\n";

    // Change output target
    std::ofstream outfile("log.txt");
    debug::log::SetOutput(outfile);
    debug::log::error("NETWORK") << "This goes to the file now.\n";

    net_dbg << "More network debug info, now in file.\n";

    return 0;
}