#include "Logger.hpp"



namespace debug {


struct LogLevelProperties {
    bool enabled = true;
    std::string colorPrefix;
    std::string colorSuffix;
};

class LineBuffer : public std::streambuf {
public:
    LineBuffer(LogLevel level, const std::string& topic)
        : m_level(level), m_topic(topic) {}

    void SetCallback(std::function<void(const std::string&)> cb) {
        m_callback = cb;
    }

    void SetEnabledCheck(std::function<bool()> enabledCheck) {
        m_enabledCheck = enabledCheck;
    }

protected:
    virtual int overflow(int c) override {
        if (c == traits_type::eof()) {
            return c;
        }

        m_buffer.push_back(static_cast<char>(c));
        if (c == '\n') {
            flushBuffer();
        }
        return c;
    }

    virtual int sync() override {
        if (!m_buffer.empty()) {
            flushBuffer();
        }
        return 0;
    }

private:
    void flushBuffer() {
        if (!m_buffer.empty()) {
            if (m_enabledCheck && m_enabledCheck()) {
                if (m_callback) {
                    m_callback(m_buffer);
                }
            }
            m_buffer.clear();
        }
    }

    std::string m_buffer;
    LogLevel m_level;
    std::string m_topic;
    std::function<void(const std::string&)> m_callback;
    std::function<bool()> m_enabledCheck;
};
    std::map<LogLevel, LogLevelProperties>& LevelProps() {
        static std::map<LogLevel, LogLevelProperties> props = {
            {LogLevel::Debug,   {true, "\033[36m", "\033[0m"}},
            {LogLevel::Info,    {true, "\033[32m", "\033[0m"}},
            {LogLevel::Warning, {true, "\033[33m", "\033[0m"}},
            {LogLevel::Error,   {true, "\033[31m", "\033[0m"}}
        };
        return props;
    }

    std::vector<std::unique_ptr<LineBuffer>>& Buffers() {
        static std::vector<std::unique_ptr<LineBuffer>> buffers;
        return buffers;
    }

    void log::RegisterTopic(const std::string& topic) {
        std::lock_guard<std::mutex> lock(Mutex());
        TopicMap()[topic] = true;
    }

    void log::SetTopicEnabled(const std::string& topic, bool enabled) {
        std::lock_guard<std::mutex> lock(Mutex());
        auto it = TopicMap().find(topic);
        if (it != TopicMap().end()) {
            it->second = enabled;
        }
    }

    void log::SetLevel(LogLevel level) {
        std::lock_guard<std::mutex> lock(Mutex());
        switch(level) {
            case LogLevel::Debug:
                LevelProps()[LogLevel::Debug].enabled = true;
                LevelProps()[LogLevel::Info].enabled = true;
                LevelProps()[LogLevel::Warning].enabled = true;
                LevelProps()[LogLevel::Error].enabled = true;
                break;
            case LogLevel::Info:
                LevelProps()[LogLevel::Debug].enabled = false;
                LevelProps()[LogLevel::Info].enabled = true;
                LevelProps()[LogLevel::Warning].enabled = true;
                LevelProps()[LogLevel::Error].enabled = true;
                break;
            case LogLevel::Warning:
                LevelProps()[LogLevel::Debug].enabled = false;
                LevelProps()[LogLevel::Info].enabled = false;
                LevelProps()[LogLevel::Warning].enabled = true;
                LevelProps()[LogLevel::Error].enabled = true;
                break;
            case LogLevel::Error:
                LevelProps()[LogLevel::Debug].enabled = false;
                LevelProps()[LogLevel::Info].enabled = false;
                LevelProps()[LogLevel::Warning].enabled = false;
                LevelProps()[LogLevel::Error].enabled = true;
                break;
        }
        
    }

    void log::EnableAllTopics(bool enabled) {
        std::lock_guard<std::mutex> lock(Mutex());
        for (auto& kv : TopicMap()) {
            kv.second = enabled;
        }
    }

    void log::EnableAllLevels(bool enabled) {
        std::lock_guard<std::mutex> lock(Mutex());
        for (auto& kv : LevelProps()) {
            kv.second.enabled = enabled;
        }
    }

    void log::SetOutput(std::ostream& stream) {
        std::lock_guard<std::mutex> lock(Mutex());
        OutputBuffer() = stream.rdbuf();
    }

    bool log::IsLevelEnabled(LogLevel level) {
        std::lock_guard<std::mutex> lock(Mutex());
        return LevelProps()[level].enabled;
    }

    bool log::IsTopicEnabled(const std::string& topic) {
        std::lock_guard<std::mutex> lock(Mutex());
        auto it = TopicMap().find(topic);
        if (it == TopicMap().end()) {
            std::cerr << "\033[31m[ERROR][LOGGER] Unregistered topic used: " << topic << "\033[0m\n";
            return false;
        }
        return it->second;
    }

    std::ostream& log::GetStream(LogLevel level, const std::string& topic) {
#if MAX_LOG_LEVEL == ERROR // ERROR only
        if (level == LogLevel::Debug || level == LogLevel::Info || level == LogLevel::Warning) {
            return NullStream();
        }
#elif MAX_LOG_LEVEL == WARNING // ERROR, WARNING
        if (level == LogLevel::Debug || level == LogLevel::Info) {
            return NullStream();
        }
#elif MAX_LOG_LEVEL == INFO // ERROR, WARNING, INFO
        if (level == LogLevel::Debug) {
            return NullStream();
        }
#endif


        std::lock_guard<std::mutex> lock(Mutex());

        auto key = std::make_pair(level, topic);
        auto it = Streams().find(key);
        if (it != Streams().end()) {
            return *it->second;
        }

        auto& props = LevelProps()[level];
        auto buffer = std::make_unique<LineBuffer>(level, topic);

        buffer->SetCallback([=](const std::string& line) {
            std::string prefix = props.colorPrefix + "[" + std::string(ToString(level)) + "][" + topic + "] ";
            std::string suffix = props.colorSuffix;
            std::string fullMessage = prefix + line + suffix;

            std::streambuf* out = OutputBuffer();
            if (out) {
                out->sputn(fullMessage.c_str(), (std::streamsize)fullMessage.size());
            }
        });

        buffer->SetEnabledCheck([=]() {
            return IsLevelEnabled(level) && IsTopicEnabled(topic);
        });

        auto stream = std::make_unique<std::ostream>(buffer.get());
        stream->rdbuf(buffer.get());

        Buffers().push_back(std::move(buffer));
        Streams()[key] = std::move(stream);
        return *Streams()[key];
    }

    std::ostream& log::NullStream() {
        static NullBuffer nb;
        static std::ostream ns(&nb);
        return ns;
    }

    std::map<std::string, bool>& log::TopicMap() {
        static std::map<std::string, bool> topics;
        return topics;
    }

    std::streambuf*& log::OutputBuffer() {
        static std::streambuf* output = nullptr;
        return output;
    }

    std::map<std::pair<LogLevel, std::string>, std::unique_ptr<std::ostream>>& log::Streams() {
        static std::map<std::pair<LogLevel, std::string>, std::unique_ptr<std::ostream>> streams;
        return streams;
    }


    std::mutex& log::Mutex() {
        static std::mutex m;
        return m;
    }

}