#include "Time.hpp"  // Include the correct header file name
#include <chrono>
#include <iostream>
#include <string>
#include <sstream>
#include <iomanip>

namespace Debug
{
    std::ostream &operator<<(std::ostream &os, const Time::Stamp &value)
    {
        std::time_t time_t_stamp = std::chrono::system_clock::to_time_t(value);
        os << std::put_time(std::localtime(&time_t_stamp), "%Y-%m-%d %H:%M:%S");
        return os;
    }

    std::ostream &operator<<(std::ostream &os, const Time::Duration &value)
    {
        os << value.count() << " seconds";
        return os;
    }

    const char* Version()
    {
        #ifndef VERSION
            #define VERSION "'embedded build'"
        #endif
        return "Debug Time Version " VERSION;
    }
}
