#ifndef H_TIME
#define H_TIME

#include <chrono>
#include <string>
#include <sstream>
#include <iomanip>


namespace Debug
{
	class Time
	{
	public:
		typedef std::chrono::time_point<std::chrono::system_clock> Stamp;
		typedef std::chrono::duration<float> Duration;

		static Stamp Now()
		{
			return std::chrono::system_clock::now();
		}
	};

	std::ostream &operator<<(std::ostream &os, const Time::Stamp &value);

	std::ostream &operator<<(std::ostream &os, const Time::Duration &value);

	const char* Version();
}

#endif