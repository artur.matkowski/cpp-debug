#ifndef _logger
#define _logger
#include <iostream>
#include <fstream>
#include <sstream>
#include <map>
#include <memory>
#include <mutex>
#include <string>
#include <vector>
#include <functional>

// ********** Configuration **********
// Define MAX_LOG_LEVEL to control compile-time log level filtering
// Allowed values: ERROR=1, WARNING=2, INFO=3, DEBUG=0 (or 4 for full debug)
// If not defined, assume full debug (0 or any other).



namespace debug {
// ********** LogLevel & Utilities **********
enum class LogLevel {
    Debug,
    Info,
    Warning,
    Error
};


static inline const char* ToString(LogLevel level) {
    switch (level) {
        case LogLevel::Debug:   return "DEBUG";
        case LogLevel::Info:    return "INFO";
        case LogLevel::Warning: return "WARNING";
        case LogLevel::Error:   return "ERROR";
    }
    return "UNKNOWN";
}


class NullBuffer : public std::streambuf {
protected:
    virtual std::streamsize xsputn(const char*, std::streamsize n) override { return n; }
    virtual int overflow(int c) override { return c; }
};

class log {
public:
    static void RegisterTopic(const std::string& topic);

    static void SetTopicEnabled(const std::string& topic, bool enabled);

    static void SetLevel(LogLevel level);

    static void EnableAllTopics(bool enabled);

    static void EnableAllLevels(bool enabled);

    static void SetOutput(std::ostream& stream);

    static bool IsLevelEnabled(LogLevel level);

    static bool IsTopicEnabled(const std::string& topic);

    static std::ostream& GetStream(LogLevel level, const std::string& topic);

    static std::ostream& NullStream();


// ********** Compile-time level control **********
// Using the requested pattern:
// If we want to call debug::log::error("TOPIC") << "Message";
// and have compile-time filtering, we define functions for each level in debug::log namespace.
// At compile-time, these functions either return Logger::GetStream or Logger::NullStream.
// The user can still enable/disable at runtime.



#define ERROR 1
#define WARNING 2
#define INFO 3
#define ALL 4

#ifndef MAX_LOG_LEVEL
#define MAX_LOG_LEVEL 0
#endif

#if MAX_LOG_LEVEL == ERROR // ERROR only
    static inline std::ostream& error(const std::string& topic)   { return GetStream(LogLevel::Error, topic); }
    static inline std::ostream& warning(const std::string&)       { return NullStream(); }
    static inline std::ostream& info(const std::string&)          { return NullStream(); }
    static inline std::ostream& debug(const std::string&)         { return NullStream(); }
#elif MAX_LOG_LEVEL == WARNING // WARNING and ERROR
    static inline std::ostream& error(const std::string& topic)   { return GetStream(LogLevel::Error, topic); }
    static inline std::ostream& warning(const std::string& topic) { return GetStream(LogLevel::Warning, topic); }
    static inline std::ostream& info(const std::string&)          { return NullStream(); }
    static inline std::ostream& debug(const std::string&)         { return NullStream(); }
#elif MAX_LOG_LEVEL == INFO // INFO, WARNING, ERROR
    static inline std::ostream& error(const std::string& topic)   { return GetStream(LogLevel::Error, topic); }
    static inline std::ostream& warning(const std::string& topic) { return GetStream(LogLevel::Warning, topic); }
    static inline std::ostream& info(const std::string& topic)    { return GetStream(LogLevel::Info, topic); }
    static inline std::ostream& debug(const std::string&)         { return NullStream(); }
#else
    // full debug mode
    static inline std::ostream& error(const std::string& topic)   { return GetStream(LogLevel::Error, topic); }
    static inline std::ostream& warning(const std::string& topic) { return GetStream(LogLevel::Warning, topic); }
    static inline std::ostream& info(const std::string& topic)    { return GetStream(LogLevel::Info, topic); }
    static inline std::ostream& debug(const std::string& topic)   { return GetStream(LogLevel::Debug, topic); }
#endif


private:

    static std::map<std::string, bool>& TopicMap();

    static std::streambuf*& OutputBuffer();

    static std::map<std::pair<LogLevel, std::string>, std::unique_ptr<std::ostream>>& Streams();


    static std::mutex& Mutex();
};

} // namespace debug



#endif